
from sqlalchemy.orm import sessionmaker
import sqlalchemy
from db_classes import Institution

engine = sqlalchemy.create_engine("sqlite:///aa.db")
engine.connect()
conn = engine.connect()
Session = sessionmaker(engine)
session = Session()

all_inst = session.query(Institution).all()

languages = set()
for inst in all_inst:
    try:
        lang = inst.language.split('\n')
    except:
        lang = []
    for l in lang:
        languages.add(l)

languages.remove('')
languages.remove('other languages with interpreters')
languages = list(languages)
languages.sort()

print(languages)

with open('../components/languages.vue', 'w') as outfile:
    outfile.write('availableLanguages= [\n"')
    outfile.write('", "'.join(languages))
    outfile.write('"]\n')

