
from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Institution(Base):
    __tablename__ = 'asylumadvice'
    id = Column(Integer, primary_key=True)
    name = Column(String(30))
    street = Column(String(50))
    street_number = Column(String(10))
    telephone_number = Column(String(50))
    email = Column(String(50))
    homepage = Column(String(50))
    opening_times = Column(String(80))
    postal_code = Column(String(10))
    city = Column(String(50))
    support_free_legal_aid = Column(Integer)
    support_paid_legal_aid = Column(Integer)
    latitude = Column(Float)
    longitude = Column(Float)
    language = Column(String(100))

