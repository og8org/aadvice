
import sqlite3
import csv
import requests


def queryCoords(street, plz_city):
    plz, city = plz_city.split()
    r = requests.get(f'https://nominatim.openstreetmap.org/search.php?street={street}&city={city}&country=germany&postalcode={plz}&format=jsonv2')
    dd = r.json()[0]
    return dd['lat'], dd['lon']


reader = csv.DictReader(open('beratungsstellen.csv'))
reader.fieldnames = [ x.strip() for x in reader.fieldnames ]


with open('../components/institutes.vue', 'w') as outfile:
    outfile.write('institutes = [\n')
    for inst in reader:
        name = inst['name'].replace('"','')
        address = inst['address'].replace('"','')
        plz_city = inst['postcode_city']
        tel = inst['telephone']
        email = inst['email']
        hours = inst['opening_hours'].replace('"','')
        web_page = inst['web_page'] or ""
        language = inst['language']
        free = "true" if inst['support_free_legal_aid'] else "false"

        lat, lon = queryCoords(address, plz_city)

        outfile.write('{')
        outfile.write(f'''name: "{name}",
                        address: "{address}",
                        postcode_city: "{plz_city}",
                        telephone: "{tel}",
                        email: "{email}",
                        web_page: "{web_page}",
                        longitude: {lon},
                        latitude: {lat},
                        hours: "{hours}",
                        language: "{language}",
                        free: {free}''')
        outfile.write('},\n')
    outfile.write(']')




# old version to read from sql file
'''
import sqlite3

conn = sqlite3.connect('aa.db')

c = conn.cursor()

try:
    c.execute("""CREATE TABLE asylumadvice
             (id integer, name text, street text, street_number text,
              telephone_number text, email text, homepage text,
              opening_times text, postal_code text, city text,
              support_free_legal_aid integer, support_paid_legal_aid integer,
              latitude real, longitude real
             )""")
except sqlite3.OperationalError:
    print('DB already exists')

with open('in.sql', 'r') as input:
    alles = input.read()
    c.execute("INSERT INTO asylumadvice VALUES " + alles)

#c.execute("""INSERT INTO asylumadvice VALUES
           #(5505, 'Interkulturelles Begegnungs- und Betreuungszentrum', 'Müll
           #erstr. 12', 0, '0371/495 127 55', 'ibbz@agiua.de', 'http://www.agiua.de', 'Mo, Di, Do, Fr: 10.00-12.00; Di: 13.00-16.00; Do:13.00-18.00', '09113', 'Saxony', 1, 0, 50.8461167, 12.9279147),
           #(5506, 'Box 66 Interkulturelles Beratungs-und Begegnungszentrum für Frauen und Familien', 'Sonntagstraße 9', 0, '030/81700540', 'mbe-box66@via-in-berlin.de', 'http://www.box66berlin.com', 'Mo – Do: 9:00–18:00 Uhr; Di–Mi: 9:00–17:00 Uhr', '10245', 'Berlin', 1, 0, 52.5057833, 13.4664648)
          #""")

conn.commit()
conn.close()
'''

