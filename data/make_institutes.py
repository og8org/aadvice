
from sqlalchemy.orm import sessionmaker
import sqlalchemy
from db_classes import Institution

engine = sqlalchemy.create_engine("sqlite:///aa.db")
engine.connect()
conn = engine.connect()
Session = sessionmaker(engine)
session = Session()

all_inst = session.query(Institution).all()

id = 0
with open('../components/institutes.vue', 'w') as outfile:
    outfile.write('institutes = [\n')
    for inst in all_inst:
        id += 1
        name = inst.name.replace('"','')
        street = inst.street.replace('"','')
        hours = inst.opening_times.replace('"','')
        homepage = inst.homepage or ""
        free = "true" if inst.support_free_legal_aid else "false"
        try:
            language = ", ".join(inst.language.split('\n'))
        except:
            language = ''

        outfile.write('{')
        outfile.write(f'id: "{id}", name: "{name}", street: "{street}", city: "{inst.city}", plz: "{inst.postal_code}", tel: "{inst.telephone_number}", email: "{inst.email}", homepage: "{homepage}", longitude: {inst.longitude}, latitude: {inst.latitude}, hours: "{hours}", free: {free}, language: "{language}"')
        outfile.write('},\n')
    outfile.write(']')

