
import csv

from sqlalchemy.orm import sessionmaker
import sqlalchemy
from db_classes import Institution

engine = sqlalchemy.create_engine("sqlite:///aa.db")
engine.connect()
conn = engine.connect()
Session = sessionmaker(engine)
session = Session()


reader = csv.DictReader(open('beratungsstellen.csv'))
reader.fieldnames = [ x.strip() for x in reader.fieldnames ]


for inst in reader:
    name = inst['name']
    street = inst['address']
    language = inst['language']

    q = session.query(Institution).filter_by(name=name, street=street)

    if q.count() == 1:
        q.first().language = language

session.commit()



