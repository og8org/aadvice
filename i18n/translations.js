
var en = en || {}
var de = de || {}
var fr = fr || {}
var ar = ar || {}
var fa = fa || {}
var tr = tr || {}

const translations = {
  en, 
  de,
  fr,
  ar,
  fa,
  tr,
}
