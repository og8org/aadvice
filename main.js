
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
        showMenu: false,
        query: '',
        center: L.latLng(START_POS_LAT, START_POS_LON),
        zoom: 11,
        bookmarks: [],
        card: null,
        selectedCard: null,
        blueheader: false,
        showLanding: true,
        showWelcome: false,
        filter: { languages: [], freeAdvice: true, paidLawyer: true }
  },
  mutations: {
    set_center(state, center) { state.center = center },
    set_zoom(state, zoom) { state.zoom = zoom },
    set_query(state, query) { state.query = query },
    set_card(state, card) { state.card = card },
    set_bookmarks(state, bm) { state.bookmarks = bm },
    set_selected(state, card) { state.selectedCard = card || null },
    set_blueheader(state, bool) { state.blueheader = bool },
    set_filter(state, filter) { state.filter = filter },
    toggle_menu(state) { state.showMenu = !state.showMenu },
    landing_done(state) { state.showLanding = false },
    welcome_done(state) { state.showWelcome = false },
    hide_menu(state) { state.showMenu = false },
    add_bookmark(state, id) { 
        if (state.bookmarks.indexOf(id) === -1) {
            state.bookmarks.push(id)
            setCookie('bookmarks', state.bookmarks.join(','));
        }
    },
    del_bookmark(state, bm) { state.bookmarks.splice(pos, 1) },
    toggle_bookmark(state, id) { 
        let pos = state.bookmarks.indexOf(id);
        if (pos === -1)
            store.commit('add_bookmark', id);
        else
            state.bookmarks.splice(pos, 1);
    },
    zoom_in(state) {
        if (state.zoom < 20)
            state.zoom++;
    },
    zoom_out(state) {
        if (state.zoom > 0)
            state.zoom--;
    }
  }
})


function get_card(id) {
  return institutes.filter(x => x.id === id)[0] || null
}

const router = new VueRouter({
  routes: [
        {
            path: '/',
            component: Home,
            beforeEnter: (to, from, next) => {
                store.commit('set_blueheader', false);
                store.commit('hide_menu');
                to.meta.title = aatitle + " - Home";
                document.title = to.meta.title;
                next();
            }
        },
        {
            path: '/search',
            component: Map,
            beforeEnter: (to, from, next) => {
                store.commit('set_blueheader', false);
                store.commit('hide_menu');
                to.meta.title = aatitle;
                document.title = to.meta.title;
                next();
            }
        },
        {
            path: '/home',
            component: Home,
            beforeEnter: (to, from, next) => {
                store.commit('set_blueheader', false);
                store.commit('hide_menu');
                to.meta.title = aatitle + " - Home";
                document.title = to.meta.title;
                next();
            }
        },
        {
            path: '/list',
            component: List,
            beforeEnter: (to, from, next) => {
                store.commit('set_blueheader', false);
                store.commit('hide_menu');
                to.meta.title = aatitle + " - List";
                document.title = to.meta.title;
                next();
            }
        },
        {
            path: '/bookmarks',
            component: Bookmarks,
            beforeEnter: (to, from, next) => {
                store.commit('set_blueheader', true);
                store.commit('hide_menu');
                to.meta.title = aatitle + " - Favorites";
                document.title = to.meta.title;
                next();
            }
        },
        {
            path: '/profile/:id',
            component: Profile,
            beforeEnter: (to, from, next) => {
                store.commit('set_blueheader', true);
                store.commit('hide_menu');
                to.meta.title = aatitle + " - Profile";
                document.title = to.meta.title;
                next();
            }
        },
        {
            path: '/error',
            component: Error,
            beforeEnter: (to, from, next) => {
                store.commit('set_blueheader', false);
                store.commit('hide_menu');
                to.meta.title = aatitle + " - Error";
                document.title = to.meta.title;
                next();
            }
        },
        {
            path: '/settings',
            component: Settings,
            beforeEnter: (to, from, next) => {
                store.commit('set_blueheader', true);
                store.commit('hide_menu');
                to.meta.title = aatitle + " - Settings";
                document.title = to.meta.title;
                next();
            }
        },
        {
            path: '/information',
            component: Information,
            beforeEnter: (to, from, next) => {
                store.commit('set_blueheader', true);
                store.commit('hide_menu');
                to.meta.title = aatitle + " - Information";
                document.title = to.meta.title;
                next();
            }
        },
        {
            path: '/about',
            component: About,
            beforeEnter: (to, from, next) => {
                store.commit('set_blueheader', true);
                store.commit('hide_menu');
                to.meta.title = aatitle + " - About";
                document.title = to.meta.title;
                next();
            }
        },
        {
            path: '/privacy',
            component: Privacy,
            beforeEnter: (to, from, next) => {
                store.commit('set_blueheader', true);
                store.commit('hide_menu');
                to.meta.title = aatitle + " - Privacy";
                document.title = to.meta.title;
                next();
            }
        },
        {
            path: '/imprint',
            component: Imprint,
            beforeEnter: (to, from, next) => {
                store.commit('set_blueheader', true);
                store.commit('hide_menu');
                to.meta.title = aatitle + " - Imprint";
                document.title = to.meta.title;
                next();
            }
        },
        {
            path: '*',
            component: Error,
            beforeEnter: (to, from, next) => {
                store.commit('set_blueheader', false);
                store.commit('hide_menu');
                to.meta.title = aatitle + " - Error";
                document.title = to.meta.title;
                next();
            }
        },
  ]
})


const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages: translations,
})


var app = new Vue({
    el: '#app',
    router,
    store,
    i18n,
    data: {},
    methods: {},
    computed: {},
    mounted() {
        let read = getCookie('bookmarks');
        if (read !== "")
            for (let bm of read.split(','))
                store.commit('add_bookmark', bm);
        else
            store.commit('set_bookmarks', []);
        let lat = getCookie('lat');
        let lon = getCookie('lon');
        if (lat !== "") {
            store.commit('set_center', L.latLng(lat, lon))
        }
        let lang = getCookie('lang');
        if (lang !== "") {
            this.$i18n.locale = lang;
            store.commit('landing_done');
        }
    }
})


window.addEventListener('beforeunload', function (e) {
        setCookie('lat', store.state.center.lat);
        setCookie('lon', store.state.center.lng);
        setCookie('lang', app.$i18n.locale);
    });


// matomo
install(Vue, {
  // Configure your matomo server and site by providing
  host: 'https://stats.asylumadvice.org',
  siteId: 1,

  // Changes the default .js and .php endpoint's filename
  // Default: 'matomo'
  trackerFileName: 'matomo',

  // Overrides the autogenerated tracker endpoint entirely
  // Default: undefined
  // trackerUrl: 'https://example.com/whatever/endpoint/you/have',

  // Overrides the autogenerated tracker script path entirely
  // Default: undefined
  // trackerScriptUrl: 'https://example.com/whatever/script/path/you/have',

  // Enables automatically registering pageviews on the router
  router: router,

  // Enables link tracking on regular links. Note that this won't
  // work for routing links (ie. internal Vue router links)
  // Default: true
  enableLinkTracking: true,

  // Require consent before sending tracking information to matomo
  // Default: false
  requireConsent: false,

  // Whether to track the initial page view
  // Default: true
  trackInitialView: true,

  // Run Matomo without cookies
  // Default: false
  disableCookies: false,

  // Require consent before creating matomo session cookie
  // Default: false
  requireCookieConsent: false,

  // Enable the heartbeat timer (https://developer.matomo.org/guides/tracking-javascript-guide#accurately-measure-the-time-spent-on-each-page)
  // Default: false
  enableHeartBeatTimer: false,

  // Set the heartbeat timer interval
  // Default: 15
  heartBeatTimerInterval: 15,

  // Whether or not to log debug information
  // Default: false
  debug: false,

  // UserID passed to Matomo (see https://developer.matomo.org/guides/tracking-javascript-guide#user-id)
  // Default: undefined
  userId: undefined,

  // Share the tracking cookie across subdomains (see https://developer.matomo.org/guides/tracking-javascript-guide#measuring-domains-andor-sub-domains)
  // Default: undefined, example '*.example.com'
  cookieDomain: undefined,

  // Tell Matomo the website domain so that clicks on these domains are not tracked as 'Outlinks'
  // Default: undefined, example: '*.example.com'
  domains: undefined,

  // A list of pre-initialization actions that run before matomo is loaded
  // Default: []
  // Example: [
  //   ['API_method_name', parameter_list],
  //   ['setCustomVariable','1','VisitorType','Member'],
  //   ['appendToTrackingUrl', 'new_visit=1'],
  //   etc.
  // ]
  preInitActions: []
});
